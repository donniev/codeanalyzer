/**
 * Walks a directory and determines the number of lines in each file
 * It also counts the number of times /require\(/ is present indicating
 * the usage of node modules. It does not walk test build bower_components or .git directories
 * and does not include underscore.js which distorts the results
 * It only analyzes js, cfc or cfm files
 * @type {Object}
 */
var cRequires = {},
  cLines = {},
  startPath = process.argv[2],
  fs = require("fs"),
  _ = require("underscore")._,
  path = require("path"),
  util = require("util"),
  proccessFile = function(pth, fname) {
    var p = pth;
    var f = fname;
    if (fname === 'underscore.js') return;
    var ext = fname.split(".");
    if ((ext[ext.length - 1] === "js") || (ext[ext.length - 1] == "cfc") || (ext[ext.length - 1] == "cfm")) {
      try {
        var content = fs.readFileSync(pth + "/" + fname, "utf8")
        var req = content.split(/require\(/).length - 1;
        var l = content.split("\n").length;
        if (!cRequires[req]) {
          cRequires[req] = [];
        }
        cRequires[req].push(fname);
        if (!cLines[l]) {
          cLines[l] = []
        }
        cLines[l].push(fname);
      } catch (ex) {
        //really do nothing this is just a non existent file
        console.log(ex);
      }
    }
  },
  walkDirectory = function(pth) {
    console.log("Walking %s", pth);
    var files = fs.readdirSync(pth);
    var p = pth;
    //  console.log(p);

    _.each(files, function(file, index) {
      var fle = file;
      try {
        var f = fs.statSync(p + "/" + fle)

        if (f.isFile()) {
          proccessFile(p, fle);
        } else if (f.isDirectory() && (fle != 'bower_components') && (fle != 'build') && (fle != '.git') && (fle != 'test') && (fle != 'node_modules')) {
          walkDirectory(p + "/" + fle);
        } else {
          console.log("not walking %s", p + "/" + fle)
        }
      } catch (ex) {
        //file system is listing a dir that isn't really there
        console.log(ex);
      }

    })
  }
console.log(startPath);
console.log(path.resolve(startPath));
walkDirectory(path.resolve(startPath));
console.log("Use of requires");
console.log(cRequires);
console.log("lines of code");
console.log(cLines);
