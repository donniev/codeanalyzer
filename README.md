#CodeAnalyzer
CodeAnalyzer is a very simple program which reports on the size of your files and their use of node modules.

It walks a directory and extracts the information and spits out the results.

It excludes the following directories
* node_modules
* build
* test
* bower_components
* .git

It only analyzes .js .cfc and .cfm files

##Usage
```
node analyzeCode.js  path/to/directory
```
Or if you want to save results
```
node analyzeCode.js path/to/repository >filename
```